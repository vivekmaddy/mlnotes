# MACHINE LEARNING BASICS

## Knowing Your Task and Knowing Your Data

While you are building a machine learning solution,
you should answer, or at least keep in mind, the following questions:

- What question(s) am I trying to answer? Do I think the data collected can answer
that question?
- What is the best way to phrase my question(s) as a machine learning problem?
- Have I collected enough data to represent the problem I want to solve?
- What features of the data did I extract, and will these enable the right
predictions?
- How will I measure success in my application?
- How will the machine learning solution interact with other parts of my research
or business product?


## Neural Networks (Deep Learning)

The terms deep learning and neural networks are used interchangeably because all deep learning systems are made of neural networks. <br>
A neural network is a method in artificial intelligence that teaches computers to process data in a way that is inspired by the human brain.



## Libraries and Tools

- scikit-learn : 
scikit-learn is built on top of
the NumPy and SciPy scientific Python libraries

- Numpy : 
NumPy is one of the fundamental packages for scientific computing in Python.
scikit-learn takes in data in the form of NumPy arrays. Any data you’re using will have to be con‐
verted to a NumPy array. 


- SciPy : 
SciPy is a collection of functions for scientific computing in Python. It provides,
among other functionality, advanced linear algebra routines, mathematical function
optimization, signal processing, special mathematical functions, and statistical distri‐
butions.


- matplotlib : 
matplotlib is the primary scientific plotting library in Python.
Using this for graph visualization.


- pandas :
pandas is a Python library for data wrangling and analysis.




## Supervised Learning
Machine learning algorithms that learn from input/output (labled data) pairs are called supervised
    learning

labled data means =>
The label data means some input data is already tagged with the correct output.
Splitting a dataset into input (features) and output (labels) groups   



## UnSupervised Learning

In unsupervised learning, only the input data is known, and no known output
data is given to the algorithm

---
<br><br> <br>



# Supervised Learning

Major types of supervised learning problems are:

- Classification 
- Regression


## Classification

In classification, the goal is to predict a class label.
Classification algorithms are used to predict outcomes like "yes or no", "spam or not spam" or 
"positive , negative or neutral" like this.

classification are in two:

- Binary classification
- Multiclass classification


    ### Binary classification
    `means predicting exactly two outcomes.`
        
    for example, "yes or no" or "spam or not".   

    ### Multiclass classification
    ` means predicting more than two outcomes.`
    for example, "positive, negative or neutral".


## Regression

In Regression, the goal is to predict a continuous number, or a floating-point
number in programming terms (or real number in mathematical terms).

for example,  Predicting a
person’s annual income from their education, their age, and where they live is an
example of a regression task.

`
An easy way to distinguish between classification and regression tasks is to ask
whether there is some kind of continuity in the output. If there is continuity between
possible outcomes, then the problem is a regression problem.
`


<br><br>

# Generalization, Overfitting, and Underfitting

### Generalization

Generalization refers to the model's ability to predict accurately on data it hasn't seen before

### Overfitting

This often happens when the model is too complex or when it's trained on a dataset with too many features relative to the number of observations.

### Underfitting

Choosing too simple a model is called underfitting.


`
There is a sweet spot in between that will yield the best generalization performance.
This is the model we want to find.
`

#





